﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Amazon.DynamoDBv2.Model;
using SensorsApi.Models;

namespace SensorsApi.Interfaces
{
    public interface IMeasurementsRepository
    {        
        Task<IEnumerable<Dictionary<string, AttributeValue>>> GetTypes();
        Task<IEnumerable<Dictionary<string, AttributeValue>>> GetMeasurements(string device, string type, string startDate, string endDate);
        Task<PutItemResponse> CreateMeasurement(MeasurementAWS measurementAWS);
    }
}
