﻿using SensorsApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorsApi.Interfaces
{
    public interface IDeviceService
    {

        Task<List<DeviceAWS>> GetDevices();

        Task InsertDevice(DeviceAWS device);

        Task DeleteDevice(DeviceAWS device);

        void ResetRepository();
    }
}
