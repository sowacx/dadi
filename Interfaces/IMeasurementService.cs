using System.Collections.Generic;
using System.Threading.Tasks;
using SensorsApi.Models;

namespace SensorsApi.Interfaces
{
#pragma warning disable CS1591

    public interface IMeasurementService
    {
        Task<IEnumerable<MeasurementAWS>> Get(Filter filter);
        Task<bool> Create(Measurement measurement);
        Task<IEnumerable<string>> GetTypes();
    }

#pragma warning restore CS1591
}