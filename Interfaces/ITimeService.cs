namespace SensorsApi.Interfaces
{
#pragma warning disable CS1591

    public interface ITimeService
    {
        string ServerTime();
    }

#pragma warning restore CS1591
}