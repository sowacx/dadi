﻿using Amazon.DynamoDBv2.Model;
using SensorsApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SensorsApi.Interfaces
{
    public interface IDevicesRepository
    {
        Task<List<DeviceAWS>> GetDevices();

        Task InsertDevice(DeviceAWS device);

        Task DeleteDevice(DeviceAWS device);

        void Refresh();
    }
}
