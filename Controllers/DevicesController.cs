using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using SensorsApi.Interfaces;
using SensorsApi.Models;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace SensorsApi.Controllers
{
#pragma warning disable CS1591

    ///<summary>
    /// This controller provides endpoint to register new device to the system
    ///</summary>    
    [Route("api/[controller]")]
    [ApiController]
    public class DevicesController : ControllerBase
    {
        private readonly IDeviceService _deviceService;
        private readonly ILogger _logger;

        public DevicesController(IDeviceService devicesService, ILogger<DevicesController> logger)
        {
            _deviceService = devicesService;
            _logger = logger;
        }

        ///<summary>
        ///This method refresh device list. It's being called DB trigger.
        ///</summary>
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpPut("refresh")]
        public IActionResult Refresh()
        {
            _deviceService.ResetRepository();
            return Ok("Refresh called");
        }

        ///<summary>
        ///This method provides server devices authentication data
        ///</summary>
        /// <remarks>
        /// Requires bearer token for authorization
        /// </remarks>
        [Authorize]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<List<DeviceAWS>> Get()
        {
            _deviceService.ResetRepository();
            var devices = _deviceService.GetDevices().Result;
            return Ok(devices);
        }

        ///<summary>
        ///Insert device's authentication data into storage
        ///</summary>
        /// <remarks>
        /// Requires bearer token for authorization
        /// </remarks>
        [Authorize]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<List<DeviceAWS>> Insert([FromBody]DeviceAWS device)
        {
            _deviceService.InsertDevice(device).Wait();
            _deviceService.ResetRepository();
            return Ok(_deviceService.GetDevices().Result);
        }


        ///<summary>
        ///Delete device's authentication data into storage
        ///</summary>
        /// <remarks>
        /// Requires bearer token for authorization
        /// </remarks>
        [Authorize]
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<List<DeviceAWS>> Delete([FromBody]DeviceAWS device)
        {
            var qwe = _deviceService.DeleteDevice(device);
            qwe.Wait();
            _deviceService.ResetRepository();
            return Ok(_deviceService.GetDevices().Result);
        }

        ///<summary>
        /// Replace Device data with another
        ///</summary>
        ///<param name="devices">Array of devicdes, element 0 would be deleted, element 1 would be inserted</param>
        /// <remarks>
        /// Requires bearer token for authorization
        /// </remarks>
        [Authorize]
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<List<DeviceAWS>> Update([FromBody]DeviceAWS[] devices)
        {
            if (devices.Length != 2)
            {
                var modelState = new ModelStateDictionary();
                modelState.AddModelError("devices", "Devices array should contain two items. [0] device to be deleted, [1] to be inserted");
                return BadRequest(modelState);
            }
            _deviceService.DeleteDevice(devices[0]).Wait();
            _deviceService.InsertDevice(devices[1]).Wait();
            _deviceService.ResetRepository();
            return Ok(_deviceService.GetDevices().Result);
        }
    }

#pragma warning restore CS1591
}