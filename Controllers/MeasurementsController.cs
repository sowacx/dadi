using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using SensorsApi.Interfaces;
using SensorsApi.Models;

namespace SensorsApi.Controllers
{
#pragma warning disable CS1591

    ///<summary>
    /// This controller provides an endpoint for putting measurements from the devices to the system
    ///</summary>
    [Route("api/[controller]")]
    [ApiController]
    public class MeasurementsController : ControllerBase
    {
        private readonly IMeasurementService _measurementService;
        private readonly ILogger _logger;

        public MeasurementsController(IMeasurementService measurementService, ILogger<MeasurementsController> logger)
        {
            _measurementService = measurementService;
            _logger = logger;
        }

       
        /// <summary>
        /// Upload measurement from device
        /// </summary>
        /// <param name="apiKeyHeader">Key specific for device</param>
        /// <param name="deviceNameHeader">Device name</param>
        /// <param name="measurement">Measurment data</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize("HasApiKey")]
        public async Task<IActionResult> Create([FromHeader(Name = "ApiKey")][Required]string apiKeyHeader,
            [FromHeader(Name = "DeviceName")][Required]string deviceNameHeader, 
            [FromBody] Measurement measurement)
        {
            var status = await _measurementService.Create(measurement) ? StatusCode(201) : StatusCode(500);
            return status;
        }
      
    }

#pragma warning restore CS1591
}