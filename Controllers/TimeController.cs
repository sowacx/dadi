using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SensorsApi.Interfaces;
using SensorsApi.Models;
using System;

namespace SensorsApi.Controllers
{
#pragma warning disable CS1591

    ///<summary>
    ///This controller provides information about server time
    ///</summary>
    [AllowAnonymous]
    [Route("api/[controller]")]
    [ApiController]
    public class TimeController : ControllerBase
    {
        private readonly ITimeService _timeService;
        private readonly ILogger _logger;

        public TimeController(ITimeService timeService, ILogger<TimeController> logger)
        {
            _timeService = timeService;
            _logger = logger;
        }

        ///<summary>
        ///This method provides server time for devices
        ///</summary>
        /// <remarks>
        /// Returns value in format:
        /// day:month:year:hour:minute:second
        /// </remarks>
        [HttpGet]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<string> Get()
        {
            _logger.LogInformation($"{DateTime.Now} => Time sent");
            return _timeService.ServerTime();
        }

        ///<summary>
        ///This method provides server time for devices
        ///</summary>
        /// <remarks>
        /// Returns value in format:
        /// epoc time in seconds (Unix type)
        /// </remarks>
        [HttpGet("epoc")]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<string> Post()
        {
            return DateTimeOffset.Now.ToUnixTimeSeconds().ToString();
        }    

    }

#pragma warning restore CS1591
}