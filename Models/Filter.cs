using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace SensorsApi.Models
{
#pragma warning disable CS1591

    public class Filter
    {
        [BsonElement("names")]
        public List<string> Names { get; set; }

        public string Type { get; set; }

        [BsonElement("startDate")]
        public DateTime StartDate { get; set; }

        [BsonElement("endDate")]
        public DateTime EndDate { get; set; }
    }

#pragma warning restore CS1591
}