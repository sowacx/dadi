using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace SensorsApi.Models
{
#pragma warning disable CS1591

    public class Device
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Name")]
        public string Name { get; set; }

        [BsonElement("Key")]
        public string Key { get; set; }

        [BsonElement("Algorithm")]
        public string Algorithm { get; set; }
    }

    public class DeviceAWS
    {
        public string DeviceName { get; set; }
        public string Token { get; set; }        
    }


#pragma warning restore CS1591
}