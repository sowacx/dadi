using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace SensorsApi.Models
{
#pragma warning disable CS1591

    public class Measurement
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Name")]
        public string Name { get; set; }

        [BsonElement("Sensors")]
        public List<Sensor> Sensors { get; set; }

        [BsonElement("Timestamp")]
        public DateTime Timestamp { get; set; }

        public class Sensor
        {
            public string Type { get; set; }

            public double Value { get; set; }
        }
    }

    public class MeasurementAWS
    {
        public string DeviceName { get; set; }
        public string CreatedAt { get; set; }
        public string MeasurementType { get; set; }
        public string MeasurementValue { get; set; }
        public long TTL { get; set; }
    }

#pragma warning restore CS1591
}