﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;

using MongoDB.Driver;

using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using SensorsApi.Interfaces;
using SensorsApi.Models;

namespace SensorsApi.Services
{
#pragma warning disable CS1591

    public class DeviceService : IDeviceService
    {        
        private readonly IDevicesRepository _devicesRepository;

        public DeviceService(IConfiguration config, IDevicesRepository devicesRepository)
        {            
            _devicesRepository = devicesRepository;
        }

        public Task<List<DeviceAWS>> GetDevices()
        {
            return _devicesRepository.GetDevices();
        }

        public Task InsertDevice(DeviceAWS device)
        {
             return _devicesRepository.InsertDevice(device);
        }

        public Task DeleteDevice(DeviceAWS device)
        {
            return _devicesRepository.DeleteDevice(device);
        }

        public void ResetRepository()
        {
            _devicesRepository.Refresh();
        }

    }

#pragma warning restore CS1591
}