using SensorsApi.Interfaces;
using System;

namespace SensorsApi.Services
{
#pragma warning disable CS1591

    public class TimeService : ITimeService
    {
        public string ServerTime()
        {
            var time = DateTime.Now;

            // return datetime in specific format
            return time.ToString("dd:MM:yy:HH:mm:ss");
        }
    }

#pragma warning restore CS1591
}