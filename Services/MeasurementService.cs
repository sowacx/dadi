using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

using MongoDB.Driver;

using SensorsApi.Interfaces;
using SensorsApi.Models;

namespace SensorsApi.Services
{
#pragma warning disable CS1591

    public class MeasurementService : IMeasurementService
    {
        private readonly IMongoCollection<Measurement> _measurements;
        private readonly IMongoCollection<Models.Type> _measurementsTypes;
        private readonly IMeasurementsRepository _measurementsRepository;

        public MeasurementService(IConfiguration config, IMeasurementsRepository measurementsRepository)
        {
            _measurementsRepository = measurementsRepository;
        }

        public async Task<IEnumerable<MeasurementAWS>> Get(Filter filter)
        {
            List<MeasurementAWS> results = new List<MeasurementAWS>();

            foreach (var device in filter.Names)
            {
                var measurement = await _measurementsRepository.GetMeasurements(device, filter.Type, filter.StartDate.ToString("yyyy-MM-ddTHH:mm:ssZ"), filter.EndDate.ToString("yyyy-MM-ddTHH:mm:ssZ"));
                foreach (var itemDict in measurement)
                {
                    var result = new MeasurementAWS();
                    result.DeviceName = itemDict["DeviceName"].S;
                    result.CreatedAt = itemDict["CreatedAt"].S;
                    result.MeasurementType = itemDict["MeasurementType"].S;
                    result.MeasurementValue = itemDict["MeasurementValue"].S;
                    results.Add(result);
                }
            }

            return results;
        }

        public async Task<bool> Create(Measurement measurement)
        {
            try
            {
                MeasurementAWS measurementAWS = new MeasurementAWS()
                {
                    DeviceName = measurement.Name,
                    CreatedAt = measurement.Timestamp.ToString("yyyy-MM-ddTHH:mm:ssZ"),
                    MeasurementType = measurement.Sensors[0].Type,
                    MeasurementValue = measurement.Sensors[0].Value.ToString()                    
                };
                await _measurementsRepository.CreateMeasurement(measurementAWS);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<IEnumerable<string>> GetTypes()
        {
            var items = await _measurementsRepository.GetTypes();
            var results = new List<string>();

            foreach (var itemDict in items)
            {
                foreach (var item in itemDict)
                {
                    if (!results.Contains(item.Value.S)) results.Add(item.Value.S);
                }
            }

            return results;
        }
    }

#pragma warning restore CS1591
}