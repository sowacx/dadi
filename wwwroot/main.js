(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-SG": "./node_modules/moment/locale/en-SG.js",
	"./en-SG.js": "./node_modules/moment/locale/en-SG.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _devices_devices_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./devices/devices.component */ "./src/app/devices/devices.component.ts");
/* harmony import */ var _user_auth_guard_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./user/auth-guard.service */ "./src/app/user/auth-guard.service.ts");
/* harmony import */ var _error_page_error_page_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./error-page/error-page.component */ "./src/app/error-page/error-page.component.ts");
/* harmony import */ var _user_signin_signin_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./user/signin/signin.component */ "./src/app/user/signin/signin.component.ts");







var appRoutes = [
    { path: '', component: _user_signin_signin_component__WEBPACK_IMPORTED_MODULE_6__["SigninComponent"] },
    { path: 'app', canActivate: [_user_auth_guard_service__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]], component: _devices_devices_component__WEBPACK_IMPORTED_MODULE_3__["DevicesComponent"] },
    {
        path: 'error',
        component: _error_page_error_page_component__WEBPACK_IMPORTED_MODULE_5__["ErrorPageComponent"],
        data: { message: 'Page not found!' }
    },
    { path: '**', redirectTo: '/error' }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(appRoutes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid h-100\">\r\n  <router-outlet></router-outlet>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@import url(\"https://fonts.googleapis.com/css?family=Montserrat:400,700\");\nbody {\n  font-family: 'Montserrat', sans-serif; }\n.footer {\n  align-self: flex-end; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQzpcXGRldlxcZGFkaVxcZGFkaS11aS9zcmNcXGFwcFxcYXBwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLHlFQUFZO0FBRVo7RUFDRSxxQ0FBcUMsRUFBQTtBQUd2QztFQUNFLG9CQUFvQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCB1cmwoJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1Nb250c2VycmF0OjQwMCw3MDAnKTtcclxuXHJcbmJvZHkge1xyXG4gIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdCcsIHNhbnMtc2VyaWY7XHJcbn1cclxuXHJcbi5mb290ZXIge1xyXG4gIGFsaWduLXNlbGY6IGZsZXgtZW5kO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_shared_services_socket_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./data/shared/services/socket.service */ "./src/app/data/shared/services/socket.service.ts");



var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'dadi-service';
    }
    AppComponent.prototype.clearLocalStorage = function (event) {
        localStorage.clear();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:onbeforeunload', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], AppComponent.prototype, "clearLocalStorage", null);
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            providers: [_data_shared_services_socket_service__WEBPACK_IMPORTED_MODULE_2__["SocketService"]],
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _devices_devices_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./devices/devices.component */ "./src/app/devices/devices.component.ts");
/* harmony import */ var _measurements_measurements_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./measurements/measurements.component */ "./src/app/measurements/measurements.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _devices_devices_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./devices/devices.service */ "./src/app/devices/devices.service.ts");
/* harmony import */ var _measurements_measurements_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./measurements/measurements.service */ "./src/app/measurements/measurements.service.ts");
/* harmony import */ var _data_data_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./data/data.component */ "./src/app/data/data.component.ts");
/* harmony import */ var _user_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./user/auth-guard.service */ "./src/app/user/auth-guard.service.ts");
/* harmony import */ var _error_page_error_page_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./error-page/error-page.component */ "./src/app/error-page/error-page.component.ts");
/* harmony import */ var _user_auth_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./user/auth.service */ "./src/app/user/auth.service.ts");
/* harmony import */ var _user_signin_signin_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./user/signin/signin.component */ "./src/app/user/signin/signin.component.ts");
















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _devices_devices_component__WEBPACK_IMPORTED_MODULE_6__["DevicesComponent"],
                _measurements_measurements_component__WEBPACK_IMPORTED_MODULE_7__["MeasurementsComponent"],
                _data_data_component__WEBPACK_IMPORTED_MODULE_11__["DataComponent"],
                _error_page_error_page_component__WEBPACK_IMPORTED_MODULE_13__["ErrorPageComponent"],
                _user_signin_signin_component__WEBPACK_IMPORTED_MODULE_15__["SigninComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"]
            ],
            providers: [_devices_devices_service__WEBPACK_IMPORTED_MODULE_9__["DevicesService"], _measurements_measurements_service__WEBPACK_IMPORTED_MODULE_10__["MeasurementsService"], _user_auth_service__WEBPACK_IMPORTED_MODULE_14__["AuthService"], _user_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__["AuthGuard"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/data/data.component.html":
/*!******************************************!*\
  !*** ./src/app/data/data.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h4 *ngIf=\"devices; else noDevicesTitle\" class=\"card-header\">\r\n  Device live data:\r\n\r\n  <select class=\"form-control\" style=\"display: inline; max-width:150px\" [(ngModel)]=\"selectedDevice\"\r\n    (ngModelChange)=\"changeDevice($event)\">\r\n    <option *ngFor=\"let dev of devices\" [ngValue]=\"dev\">{{ dev.Name }}</option>\r\n  </select>\r\n</h4>\r\n<ng-template #noDevicesTitle>\r\n  <h4 class=\"card-header\">Device live data</h4>\r\n</ng-template>\r\n\r\n<div class=\"card-body\">\r\n  <div>\r\n    <div *ngIf=\"devices; else noDevices\">\r\n      <div class=\"devices-data-wrapper\">\r\n        <div class=\"devices-data-item\">\r\n          <div class=\"devices-data-text\">\r\n            CO Level: {{ this.measurementsSet.COLevel }} ppm\r\n          </div>\r\n          <div class=\"devices-data-chart\">\r\n            <canvas id=\"chartOfCOLevel\">{{\r\n              this.charts[this.chartArray[COLevel]]\r\n            }}</canvas>\r\n          </div>\r\n        </div>\r\n        <div class=\"line\"></div>\r\n        <div class=\"devices-data-item\">\r\n          <div class=\"devices-data-text\">\r\n            O2 Level: {{ this.measurementsSet.O2Level }} %\r\n          </div>\r\n          <div class=\"devices-data-chart\">\r\n            <canvas id=\"chartOfO2Level\">{{\r\n              this.charts[this.chartArray[O2Level]]\r\n            }}</canvas>\r\n          </div>\r\n        </div>\r\n        <div class=\"line\"></div>\r\n        <div class=\"devices-data-item\">\r\n          <div class=\"devices-data-text\">\r\n            CO2 Level: {{ this.measurementsSet.CO2Level }} ppm\r\n          </div>\r\n          <div class=\"devices-data-chart\">\r\n            <canvas id=\"chartOfCO2Level\">{{\r\n              this.charts[this.chartArray[CO2Level]]\r\n            }}</canvas>\r\n          </div>\r\n        </div>\r\n        <div class=\"line\"></div>\r\n        <div class=\"devices-data-item\">\r\n          <div class=\"devices-data-text\">\r\n            NO2 Level: {{ this.measurementsSet.NO2Level }} ppm\r\n          </div>\r\n          <div class=\"devices-data-chart\">\r\n            <canvas id=\"chartOfNO2Level\">{{\r\n              this.charts[this.chartArray[NO2Level]]\r\n            }}</canvas>\r\n          </div>\r\n        </div>\r\n        <div class=\"line\"></div>\r\n        <div class=\"devices-data-item\">\r\n          <div class=\"devices-data-text\">\r\n            AirQuality: {{ this.measurementsSet.AirQuality }}\r\n          </div>\r\n          <div class=\"devices-data-chart\">\r\n            <canvas id=\"chartOfAirQuality\">{{\r\n              this.charts[this.chartArray[AirQuality]]\r\n            }}</canvas>\r\n          </div>\r\n        </div>\r\n        <div class=\"line\"></div>\r\n        <div class=\"devices-data-item\">\r\n          <div class=\"devices-data-text\">\r\n            VisibleLight: {{ this.measurementsSet.VisibleLight }} lm\r\n          </div>\r\n          <div class=\"devices-data-chart\">\r\n            <canvas id=\"chartOfVisibleLight\">{{\r\n              this.charts[this.chartArray[VisibleLight]]\r\n            }}</canvas>\r\n          </div>\r\n        </div>\r\n        <div class=\"line\"></div>\r\n        <div class=\"devices-data-item\">\r\n          <div class=\"devices-data-text\">\r\n            Luminance: {{ this.measurementsSet.Luminance }} Lux\r\n          </div>\r\n          <div class=\"devices-data-chart\">\r\n            <canvas id=\"chartOfLuminance\">{{\r\n              this.charts[this.chartArray[Luminance]]\r\n            }}</canvas>\r\n          </div>\r\n        </div>\r\n        <div class=\"line\"></div>\r\n        <div class=\"devices-data-item\">\r\n          <div class=\"devices-data-text\">\r\n            InfraredLight: {{ this.measurementsSet.InfraredLight }} lm\r\n          </div>\r\n          <div class=\"devices-data-chart\">\r\n            <canvas id=\"chartOfInfraredLight\">{{\r\n              this.charts[this.chartArray[InfraredLight]]\r\n            }}</canvas>\r\n          </div>\r\n        </div>\r\n        <div class=\"line\"></div>\r\n        <div class=\"devices-data-item\">\r\n          <div class=\"devices-data-text\">\r\n            Humidity: {{ this.measurementsSet.Humidity }} %\r\n          </div>\r\n          <div class=\"devices-data-chart\">\r\n            <canvas id=\"chartOfHumidity\">{{\r\n              this.charts[this.chartArray[Humidity]]\r\n            }}</canvas>\r\n          </div>\r\n        </div>\r\n        <div class=\"line\"></div>\r\n        <div class=\"devices-data-item\">\r\n          <div class=\"devices-data-text\">\r\n            Temperature: {{ this.measurementsSet.Temperature }} °C\r\n          </div>\r\n          <div class=\"devices-data-chart\">\r\n            <canvas id=\"chartOfTemperature\">{{\r\n              this.charts[this.chartArray[Temperature]]\r\n            }}</canvas>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <ng-template #noDevices>\r\n      <div class=\"alert alert-primary\">\r\n        Currently there is no device selected!\r\n      </div>\r\n    </ng-template>\r\n\r\n    <ng-template #noData>\r\n      <div class=\"alert alert-primary\">\r\n        Currently there is no data from {{ device }}!\r\n      </div>\r\n    </ng-template>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/data/data.component.scss":
/*!******************************************!*\
  !*** ./src/app/data/data.component.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".devices-data-item {\n  display: flex;\n  flex-flow: row wrap;\n  justify-content: space-between;\n  align-items: center;\n  padding: 5px 0px; }\n\n.devices-data-text {\n  text-align: left;\n  font-size: 0.8em; }\n\n.devices-data-chart {\n  overflow: hidden;\n  width: 250px;\n  height: 62px; }\n\n.devices-data-chart > canvas {\n  width: 250px;\n  height: 62px; }\n\n.line {\n  height: 3px;\n  border-top: 1px rgba(0, 0, 0, 0.125) solid; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGF0YS9DOlxcZGV2XFxkYWRpXFxkYWRpLXVpL3NyY1xcYXBwXFxkYXRhXFxkYXRhLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQiw4QkFBOEI7RUFDOUIsbUJBQW1CO0VBQ25CLGdCQUFnQixFQUFBOztBQUdsQjtFQUNFLGdCQUFnQjtFQUNoQixnQkFBZ0IsRUFBQTs7QUFHbEI7RUFDRSxnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLFlBQVksRUFBQTs7QUFHZDtFQUNFLFlBQVk7RUFDWixZQUFZLEVBQUE7O0FBR2Q7RUFDRSxXQUFXO0VBQ1gsMENBQTBDLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9kYXRhL2RhdGEuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZGV2aWNlcy1kYXRhLWl0ZW0ge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1mbG93OiByb3cgd3JhcDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBwYWRkaW5nOiA1cHggMHB4O1xyXG59XHJcblxyXG4uZGV2aWNlcy1kYXRhLXRleHQge1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgZm9udC1zaXplOiAwLjhlbTtcclxufVxyXG5cclxuLmRldmljZXMtZGF0YS1jaGFydCB7XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICB3aWR0aDogMjUwcHg7XHJcbiAgaGVpZ2h0OiA2MnB4O1xyXG59XHJcblxyXG4uZGV2aWNlcy1kYXRhLWNoYXJ0PmNhbnZhcyB7XHJcbiAgd2lkdGg6IDI1MHB4O1xyXG4gIGhlaWdodDogNjJweDtcclxufVxyXG5cclxuLmxpbmUge1xyXG4gIGhlaWdodDogM3B4O1xyXG4gIGJvcmRlci10b3A6IDFweCByZ2JhKDAsIDAsIDAsIDAuMTI1KSBzb2xpZDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/data/data.component.ts":
/*!****************************************!*\
  !*** ./src/app/data/data.component.ts ***!
  \****************************************/
/*! exports provided: DataComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataComponent", function() { return DataComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _devices_devices_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../devices/devices.service */ "./src/app/devices/devices.service.ts");
/* harmony import */ var _shared_services_socket_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shared/services/socket.service */ "./src/app/data/shared/services/socket.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var chart_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! chart.js */ "./node_modules/chart.js/dist/Chart.js");
/* harmony import */ var chart_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(chart_js__WEBPACK_IMPORTED_MODULE_5__);






var SOCKET_URL_ROOT = "wss://mfela12gfg.execute-api.eu-central-1.amazonaws.com/test?deviceName=";
var DataComponent = /** @class */ (function () {
    function DataComponent(devicesService, socketService) {
        var _this = this;
        this.devicesService = devicesService;
        this.socketService = socketService;
        this.title = "Devices";
        //availableDevices: Device[];
        this.measurementsSet = {
            COLevel: 0,
            Humidity: 0,
            CO2Level: 0,
            NO2Level: 0,
            O2Level: 0,
            Temperature: 0,
            VisibleLight: 0,
            AirQuality: 0,
            InfraredLight: 0,
            Luminance: 0
        };
        this.currentSet = {
            COLevel: 0,
            Humidity: 0,
            CO2Level: 0,
            NO2Level: 0,
            O2Level: 0,
            Temperature: 0,
            VisibleLight: 0,
            AirQuality: 0,
            InfraredLight: 0,
            Luminance: 0
        };
        this.chartDataSets = Array();
        this.chartArray = {};
        this.charts = [];
        this.oberserableTimer();
        this.devicesService.change.subscribe(function (selectedDevices) {
            _this.devices = selectedDevices;
            _this.selectedDevice = selectedDevices[0];
            _this.connectToWS();
        });
        this.measurementsSet;
        this.colors = [
            "#4dc9f6",
            "#f67019",
            "#f53794",
            "#537bc4",
            "#acc236",
            "#166a8f",
            "#00a950",
            "#58595b",
            "#8549ba"
        ];
    }
    DataComponent.prototype.oberserableTimer = function () {
        var _this = this;
        var source = Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["timer"])(1000, 10000);
        var abc = source.subscribe(function (val) {
            _this.updateVisibleElements();
        });
    };
    DataComponent.prototype.createDataSet = function (name, color) {
        var newDataSet = {
            name: name,
            label: name,
            data: Array(50).fill(null),
            fill: false,
            backgroundColor: color,
            borderColor: color,
            borderWidth: 1,
            pointRadius: 0
        };
        this.chartDataSets.push(newDataSet);
        return newDataSet;
    };
    DataComponent.prototype.createChart = function (name) {
        if (this.chartArray[name] != null) {
            return this.charts[this.chartArray[name]];
        }
        var element = document.getElementById("chartOf" + name);
        var lineColor = this.colors.pop();
        var newChart = new chart_js__WEBPACK_IMPORTED_MODULE_5__["Chart"](element, {
            type: "line",
            options: {
                responsive: false,
                maintainAspectRatio: true,
                aspectRatio: 4,
                animation: {
                    duration: 100 // general animation time
                },
                hover: {
                    animationDuration: 0 // duration of animations when hovering an item
                },
                responsiveAnimationDuration: 100,
                legend: { display: false },
                scales: {
                    xAxes: [
                        {
                            display: true,
                            ticks: {
                                minRotation: -5,
                                maxRotation: -5,
                                autoSkip: false,
                                callback: function (value, index, values) {
                                    if (index === 10) {
                                        return "-5 min. ago ";
                                    }
                                    else if (index + 1 >= values.length) {
                                        return "Now";
                                    }
                                }
                            },
                            gridLines: {
                                display: false
                            }
                        }
                    ],
                    yAxes: [
                        {
                            display: true,
                            position: "right",
                            stacked: false,
                            ticks: {
                                maxTicksLimit: 2
                            }
                        }
                    ]
                }
            },
            data: {
                labels: Array(50)
                    .fill("0")
                    .fill("-5 mins", 0, 1)
                    .fill("Now", 49, 50),
                showLines: true,
                datasets: [this.createDataSet(name, lineColor)]
            }
        });
        this.chartArray[name] = this.charts.length;
        this.charts.push(newChart);
        return newChart;
    };
    DataComponent.prototype.ngOnInit = function () { };
    DataComponent.prototype.ngOnChanges = function (changes) { };
    DataComponent.prototype.changeDevice = function (event) {
        //Clear stored data
        this.measurementsSet = {
            COLevel: 0,
            Humidity: 0,
            CO2Level: 0,
            NO2Level: 0,
            O2Level: 0,
            Temperature: 0,
            VisibleLight: 0,
            AirQuality: 0,
            InfraredLight: 0,
            Luminance: 0
        };
        this.currentSet = {
            COLevel: 0,
            Humidity: 0,
            CO2Level: 0,
            NO2Level: 0,
            O2Level: 0,
            Temperature: 0,
            VisibleLight: 0,
            AirQuality: 0,
            InfraredLight: 0,
            Luminance: 0
        };
        //clear charts data
        for (var _i = 0, _a = this.charts; _i < _a.length; _i++) {
            var item = _a[_i];
            var chartx = item;
            chartx.data.datasets[0].data = Array(50).fill(null);
        }
        //Set up new WS connection
        this.socketService.disconnect();
        this.connectToWS();
    };
    DataComponent.prototype.connectToWS = function () {
        var _this = this;
        var SocketURL = SOCKET_URL_ROOT + this.selectedDevice.Name;
        this.messages = (this.socketService
            .connect(SocketURL)
            .map(function (response) {
            var data = JSON.parse(response.data);
            if (data instanceof Array) {
                var newValues = data.map(function (item) {
                    return {
                        createdAt: item.CreatedAt.S,
                        deviceName: item.DeviceName.S,
                        measurementType: item.MeasurementType.S,
                        measurementValue: item.MeasurementValue.S
                    };
                });
                return newValues;
            }
        }));
        this.messages.subscribe(function (msg) {
            for (var _i = 0, msg_1 = msg; _i < msg_1.length; _i++) {
                var measurementMsg = msg_1[_i];
                if (!_this.chartArray[measurementMsg.measurementType]) {
                    _this.createChart(measurementMsg.measurementType);
                }
                _this.currentSet[measurementMsg.measurementType] =
                    measurementMsg.measurementValue;
            }
        });
        // }
    };
    DataComponent.prototype.updateVisibleElements = function () {
        for (var type in this.currentSet) {
            var stringVal = this.currentSet[type].toString();
            if (stringVal.indexOf(".") > -1) {
                stringVal = parseFloat(stringVal).toFixed(2);
            }
            this.measurementsSet[type] = stringVal;
            var chart = this.charts[this.chartArray[type]];
            // if (msg[0].measurementType === "Temperature") {
            if (chart) {
                chart.data.datasets[0].data.splice(0, 1);
                chart.data.datasets[0].data.push(this.currentSet[type]);
                chart.update();
            }
        }
    };
    DataComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-data",
            template: __webpack_require__(/*! ./data.component.html */ "./src/app/data/data.component.html"),
            styles: [__webpack_require__(/*! ./data.component.scss */ "./src/app/data/data.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_devices_devices_service__WEBPACK_IMPORTED_MODULE_2__["DevicesService"],
            _shared_services_socket_service__WEBPACK_IMPORTED_MODULE_3__["SocketService"]])
    ], DataComponent);
    return DataComponent;
}());



/***/ }),

/***/ "./src/app/data/shared/services/socket.service.ts":
/*!********************************************************!*\
  !*** ./src/app/data/shared/services/socket.service.ts ***!
  \********************************************************/
/*! exports provided: SocketService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SocketService", function() { return SocketService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm5/Rx.js");



var SocketService = /** @class */ (function () {
    function SocketService() {
    }
    SocketService.prototype.connect = function (url) {
        if (!this.subject) {
            this.subject = this.create(url);
            console.log('Successfully connected: ' + url);
        }
        return this.subject;
    };
    SocketService.prototype.disconnect = function () {
        if (this.subject) {
            this.ws.close();
            this.subject = null;
        }
    };
    SocketService.prototype.create = function (url) {
        var ws = new WebSocket(url);
        var observable = rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__["Observable"].create(function (obs) {
            ws.onmessage = obs.next.bind(obs);
            ws.onerror = obs.error.bind(obs);
            ws.onclose = obs.complete.bind(obs);
            return ws.close.bind(ws);
        });
        var observer = {
            next: function (data) { }
        };
        this.ws = ws;
        return rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__["Subject"].create(observer, observable);
    };
    SocketService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SocketService);
    return SocketService;
}());



/***/ }),

/***/ "./src/app/devices/devices.component.html":
/*!************************************************!*\
  !*** ./src/app/devices/devices.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"h-100\">\r\n  <div class=\"row header\">\r\n    <div class=\"col\">\r\n      <nav id=\"nav-card\" class=\"header navbar navbar-expand-lg \">\r\n        <span class=\"p-3 logo\">{{title}}</span>\r\n        <button type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" data-target=\"#devicesModalScrollable\">\r\n          Select devices\r\n        </button>\r\n        <span class=\"spacer\"></span>\r\n        <span id=\"email-place\">{{userEmail}}</span>\r\n        <button type=\"button\" class=\"btn btn-outline-secondary\" (click)=\"onLogout()\">\r\n          Log out\r\n        </button>\r\n      </nav>\r\n    </div>\r\n  </div>\r\n  <div class=\"row content\">\r\n    <div id=\"measurements-card\" class=\"col-md-12 content measurementsContainer\">\r\n      <app-measurements></app-measurements>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<ng-template #noDevices>\r\n  <button class=\"btn btn-danger\" disabled>No divices</button>\r\n</ng-template>\r\n\r\n<div class=\"modal fade\" id=\"devicesModalScrollable\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"devicesModalScrollable\"\r\n  aria-hidden=\"true\">\r\n  <div class=\"modal-dialog modal-dialog-centered modal-lg\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5 class=\"modal-title\" id=\"devicesModalScrollable\">Choose devices</h5>\r\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n          <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <div class=\"container-fluid\">\r\n          <div class=\"row\">\r\n            <div class=\"col\">\r\n              <div class=\"custom-control custom-checkbox\" *ngFor=\"let device of devices\">\r\n                <input type=\"checkbox\" class=\"custom-control-input\" [id]=device (change)=\"onDeviceSelect(device)\">\r\n                <label class=\"custom-control-label\" [for]=device>{{device}}</label>\r\n              </div>\r\n            </div>\r\n            <div class=\"col\">\r\n              <span>Selected type:</span><br>\r\n              <span>{{chartType}}</span>\r\n              <div class=\"btn-group\">\r\n                <!-- <button class=\"btn btn-secondary btn-sm\" type=\"button\">\r\n                  <span *ngIf=\"!chartType; else chartTypeTemplate\">Select measurements type</span>\r\n                </button>\r\n                <button type=\"button\" class=\"btn btn-secondary btn-sm dropdown-toggle dropdown-toggle-split\"\r\n                  data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n                  <span class=\"sr-only\">Toggle Dropdown</span>\r\n                </button> -->\r\n                <button class=\"btn btn-secondary btn-sm dropdown-toggle\" type=\"button\" id=\"dropdownMenuButton\"\r\n                  data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Select measurement type</button>\r\n                <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton\">\r\n                  <a class=\"dropdown-item\" *ngFor=\"let item of typeList\" (click)=\"onTypeSelect(item)\">{{item}}</a>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col\" id=\"datepickerContainer\">\r\n              <label for=\"startDate\">From date: </label><br>\r\n              <input type=\"date\" id=\"startDate\" class=\"date-picker picker\" [(ngModel)]=\"startDate\"\r\n                max=\"{{maxStartDate}}\" min=\"{{minStartDate}}\"><br>\r\n              <input type=\"time\" class=\"picker\" step=\"1\" [(ngModel)]=\"startTime\"><br><br>\r\n              <label for=\"endDate\">To date: </label><br>\r\n              <input type=\"date\" id=\"endDate\" class=\"date-picker picker\" [(ngModel)]=\"endDate\" max=\"{{maxEndDate}}\"\r\n                min=\"{{minEndDate}}\"><br>\r\n              <input type=\"time\" class=\"picker\" step=\"1\" [(ngModel)]=\"endTime\"><br>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\" data-toggle=\"tooltip\" data-placement=\"bottom\"\r\n          title=\"Exports selected data to the file\" (click)=\"onDataExport()\">Export data</button>\r\n        <button type=\"button\" class=\"btn btn-outline-primary\" data-dismiss=\"modal\" (click)=\"onSubmit()\">Submit</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<ng-template #chartTypeTemplate>\r\n  <span>Type: {{chartType}}</span>\r\n</ng-template>\r\n"

/***/ }),

/***/ "./src/app/devices/devices.component.scss":
/*!************************************************!*\
  !*** ./src/app/devices/devices.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#nav-card {\n  background-image: linear-gradient(to right, #263238, #78909C);\n  color: #ECEFF1;\n  padding: 0.5em; }\n\n.spacer {\n  flex: 1 1 auto; }\n\n.function-header {\n  min-height: 2vh;\n  padding: 0.5em; }\n\n.date-holder {\n  margin-right: 0.5em; }\n\n.devices-holder {\n  margin-right: 1rem; }\n\n.logo {\n  font-size: 1.2em; }\n\n.date-picker {\n  margin-bottom: 0.5em; }\n\n.picker {\n  border: none;\n  border-bottom: #263238 solid 1px; }\n\n#email-place {\n  margin-right: 0.5em; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGV2aWNlcy9DOlxcZGV2XFxkYWRpXFxkYWRpLXVpL3NyY1xcYXBwXFxkZXZpY2VzXFxkZXZpY2VzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsNkRBQTZEO0VBQzdELGNBQWM7RUFDZCxjQUFjLEVBQUE7O0FBR2hCO0VBQ0UsY0FBYyxFQUFBOztBQUdoQjtFQUNFLGVBQWU7RUFDZixjQUFjLEVBQUE7O0FBR2hCO0VBQ0UsbUJBQW1CLEVBQUE7O0FBR3JCO0VBQ0Usa0JBQWtCLEVBQUE7O0FBR3BCO0VBQ0UsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBQ0Usb0JBQW9CLEVBQUE7O0FBR3RCO0VBQ0UsWUFBWTtFQUNaLGdDQUFnQyxFQUFBOztBQUdsQztFQUNFLG1CQUNGLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9kZXZpY2VzL2RldmljZXMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjbmF2LWNhcmQge1xyXG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzI2MzIzOCwgIzc4OTA5Qyk7XHJcbiAgY29sb3I6ICNFQ0VGRjE7XHJcbiAgcGFkZGluZzogMC41ZW07XHJcbn1cclxuXHJcbi5zcGFjZXIge1xyXG4gIGZsZXg6IDEgMSBhdXRvO1xyXG59XHJcblxyXG4uZnVuY3Rpb24taGVhZGVyIHtcclxuICBtaW4taGVpZ2h0OiAydmg7XHJcbiAgcGFkZGluZzogMC41ZW07XHJcbn1cclxuXHJcbi5kYXRlLWhvbGRlciB7XHJcbiAgbWFyZ2luLXJpZ2h0OiAwLjVlbTtcclxufVxyXG5cclxuLmRldmljZXMtaG9sZGVyIHtcclxuICBtYXJnaW4tcmlnaHQ6IDFyZW07XHJcbn1cclxuXHJcbi5sb2dvIHtcclxuICBmb250LXNpemU6IDEuMmVtO1xyXG59XHJcblxyXG4uZGF0ZS1waWNrZXIge1xyXG4gIG1hcmdpbi1ib3R0b206IDAuNWVtO1xyXG59XHJcblxyXG4ucGlja2VyIHtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgYm9yZGVyLWJvdHRvbTogIzI2MzIzOCBzb2xpZCAxcHg7XHJcbn1cclxuXHJcbiNlbWFpbC1wbGFjZSB7XHJcbiAgbWFyZ2luLXJpZ2h0OiAwLjVlbVxyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/devices/devices.component.ts":
/*!**********************************************!*\
  !*** ./src/app/devices/devices.component.ts ***!
  \**********************************************/
/*! exports provided: DevicesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DevicesComponent", function() { return DevicesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _devices_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./devices.service */ "./src/app/devices/devices.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _user_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../user/auth.service */ "./src/app/user/auth.service.ts");



/**
 * To type a variable or property as Moment in Typescript you can do this e.g.:
 * let myMoment: moment.Moment = moment("someDate");
 */


var DevicesComponent = /** @class */ (function () {
    function DevicesComponent(devicesService, authService) {
        this.devicesService = devicesService;
        this.authService = authService;
        this.isAuthenticated = false;
        this.title = 'DADI Service';
        this.selectedDevices = [];
        this.filter = {
            Names: [],
            Type: '',
            StartDate: '',
            EndDate: ''
        };
        this.startDate = '--.--.----';
        this.startTime = '--:--:--';
        this.endDate = '--.--.----';
        this.endTime = '--:--:--';
        this.now = Date.now();
        this.lastDays = new Date();
        this.maxStartDate = moment__WEBPACK_IMPORTED_MODULE_3__(this.now).format('YYYY-MM-DD');
        this.maxEndDate = moment__WEBPACK_IMPORTED_MODULE_3__(this.now).format('YYYY-MM-DD');
    }
    DevicesComponent.prototype.ngOnInit = function () {
        var _this = this;
        // // this.logger.trace(`${new Date()}: Devices component ngOnInit`);
        this.devicesObsSubscription = this.devicesService
            .getDevices()
            .subscribe(function (data) {
            _this.devices = data; // devices get from the back-end
        });
        this.measurementTypes = this.devicesService
            .getMeasurementTypes()
            .subscribe(function (data) {
            _this.typeList = data;
        });
        this.userEmail = localStorage.getItem('dadi-user');
    };
    /**
     * This method change array of devices that are selected
     * @param name name of the device that is selected
     */
    DevicesComponent.prototype.onDeviceSelect = function (name) {
        var _this = this;
        if (!this.selectedDevices.includes(name)) {
            this.selectedDevices.push(name);
        }
        else {
            this.selectedDevices.splice(this.selectedDevices.indexOf(name), 1);
        }
        var selectedDevicesR = this.devices.filter(function (device) {
            return _this.selectedDevices.includes(device.Name);
        });
    };
    DevicesComponent.prototype.onSubmit = function () {
        this.filter.Names = this.selectedDevices;
        if (this.startDate === '--.--.----') {
            this.startDate = moment__WEBPACK_IMPORTED_MODULE_3__(this.now).format('YYYY-MM-DD');
        }
        if (this.startTime === '--:--:--') {
            this.startTime = '00:00:00';
        }
        if (this.endDate === '--.--.----') {
            this.endDate = moment__WEBPACK_IMPORTED_MODULE_3__(this.now).format('YYYY-MM-DD');
        }
        if (this.endTime === '--:--:--') {
            this.endTime = '23:59:59';
        }
        this.filter.StartDate = this.startDate + " " + this.startTime;
        this.filter.EndDate = this.endDate + " " + this.endTime;
        this.filter.Type = '';
        this.devicesService.searchMeasurements(this.filter);
        this.devicesService.setUserSelectedDevices(this.selectedDevices.map(function (item) {
            return {
                Name: item
            };
        }));
    };
    DevicesComponent.prototype.ngOnDestroy = function () {
        // this.devicesObsSubscription.unsubscribe();
        // this.userSubscription.unsubscribe();
        // localStorage.clear();
    };
    DevicesComponent.prototype.onTypeSelect = function (type) {
        this.devicesService.onTypeSelect(type);
        this.chartType = type;
    };
    DevicesComponent.prototype.onLogout = function () {
        this.authService.logout();
        localStorage.clear();
    };
    DevicesComponent.prototype.onDataExport = function () {
        this.devicesService.onDataExport();
    };
    DevicesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-devices',
            template: __webpack_require__(/*! ./devices.component.html */ "./src/app/devices/devices.component.html"),
            styles: [__webpack_require__(/*! ./devices.component.scss */ "./src/app/devices/devices.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_devices_service__WEBPACK_IMPORTED_MODULE_2__["DevicesService"],
            _user_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], DevicesComponent);
    return DevicesComponent;
}());



/***/ }),

/***/ "./src/app/devices/devices.service.ts":
/*!********************************************!*\
  !*** ./src/app/devices/devices.service.ts ***!
  \********************************************/
/*! exports provided: DevicesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DevicesService", function() { return DevicesService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _measurements_measurements_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../measurements/measurements.service */ "./src/app/measurements/measurements.service.ts");
/* harmony import */ var _user_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../user/auth.service */ "./src/app/user/auth.service.ts");







var DevicesService = /** @class */ (function () {
    function DevicesService(httpClient, measurements, authService) {
        this.httpClient = httpClient;
        this.measurements = measurements;
        this.authService = authService;
        this.env = _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"];
        this.url = '/devices';
        this.change = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    /**
     * this method sends request to the server for devices
     * next it maps response to the array of devices
     */
    DevicesService.prototype.getDevices = function () {
        var reqUrl = '';
        if (this.env.production) {
            reqUrl = this.url;
        }
        else {
            reqUrl = "" + this.env.protocol + this.env.host + this.url;
        }
        var token = '';
        this.authService.getAuthenticatedUser().getSession(function (err, session) {
            if (err) {
                return;
            }
            token = session.getIdToken();
        });
        return this.httpClient
            .get(reqUrl, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]().set('Authorization', token)
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (devices) {
            return devices;
        }));
    };
    DevicesService.prototype.getMeasurementTypes = function () {
        return this.measurements.getMeasurementTypes();
    };
    /**
     * This method takes options from modal and send it to the
     * measurements services where http post request is sending to the back-end
     * @param filter filtering options for request to the server
     * (list of devices, starting date and ending date)
     */
    DevicesService.prototype.searchMeasurements = function (filter) {
        var _this = this;
        this.measurements.getMeasurementsByFilter(filter).subscribe(function (measurementsRes) {
            _this.measurements.setMeasurements(measurementsRes);
        }, function (error) {
            console.error(error);
        });
    };
    /**
     * This method is invoked when any device is selected from the devices
     * @param devices Array of the selected devices in modal in ui
     */
    DevicesService.prototype.selectedDevices = function (devices) {
        this.devices = devices;
    };
    DevicesService.prototype.setUserSelectedDevices = function (devices) {
        this.userSelectedDevices = devices;
        this.change.emit(this.userSelectedDevices);
    };
    DevicesService.prototype.onTypeSelect = function (type) {
        this.measurements.measurementType = type;
    };
    DevicesService.prototype.setMeasurementsToExport = function (data) {
        this.data = data;
    };
    DevicesService.prototype.onDataExport = function () {
        var csvHeader = 'Device;Timestamp;Type;Value\n';
        var data = '';
        if (this.data == null || !this.data) {
            alert('There is no data to export');
            return;
        }
        for (var _i = 0, _a = this.data; _i < _a.length; _i++) {
            var item = _a[_i];
            data += item.DeviceName.S + ";" + item.CreatedAt.S + ";" + item.MeasurementType.S + ";" + item.MeasurementValue.S + "\n";
        }
        var date = new Date();
        var filename = this.data[0].DeviceName.S + "_" + this.data[0].MeasurementType.S + "_" + date.toISOString();
        var file = new Blob([csvHeader, data], { type: 'text/csv' });
        var a = document.createElement('a');
        var url = URL.createObjectURL(file);
        a.href = url;
        a.download = filename.toString() + ".csv";
        document.body.appendChild(a);
        a.click();
        setTimeout(function () {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);
        }, 0);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], DevicesService.prototype, "change", void 0);
    DevicesService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            _measurements_measurements_service__WEBPACK_IMPORTED_MODULE_5__["MeasurementsService"],
            _user_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]])
    ], DevicesService);
    return DevicesService;
}());



/***/ }),

/***/ "./src/app/error-page/error-page.component.html":
/*!******************************************************!*\
  !*** ./src/app/error-page/error-page.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h4>{{ errorMessage }}</h4>\r\n"

/***/ }),

/***/ "./src/app/error-page/error-page.component.sass":
/*!******************************************************!*\
  !*** ./src/app/error-page/error-page.component.sass ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Vycm9yLXBhZ2UvZXJyb3ItcGFnZS5jb21wb25lbnQuc2FzcyJ9 */"

/***/ }),

/***/ "./src/app/error-page/error-page.component.ts":
/*!****************************************************!*\
  !*** ./src/app/error-page/error-page.component.ts ***!
  \****************************************************/
/*! exports provided: ErrorPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorPageComponent", function() { return ErrorPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var ErrorPageComponent = /** @class */ (function () {
    function ErrorPageComponent(route) {
        this.route = route;
    }
    ErrorPageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.data.subscribe(function (data) {
            _this.errorMessage = data.message;
        });
    };
    ErrorPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-error-page',
            template: __webpack_require__(/*! ./error-page.component.html */ "./src/app/error-page/error-page.component.html"),
            styles: [__webpack_require__(/*! ./error-page.component.sass */ "./src/app/error-page/error-page.component.sass")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], ErrorPageComponent);
    return ErrorPageComponent;
}());



/***/ }),

/***/ "./src/app/measurements/measurements.component.html":
/*!**********************************************************!*\
  !*** ./src/app/measurements/measurements.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"p-3 content\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-5 col-lg-3\">\r\n      <div class=\"card card-padding\">\r\n        <app-data></app-data>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"col-md-7 col-lg-9\">\r\n      <div class=\"card card-padding\">\r\n        <h4 class=\"card-header\">Measurements</h4>\r\n        <div id=\"appChartContainer\" class=\"card-body\">\r\n          <div *ngIf=\"measurements; else noDevices\"></div>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-2\">\r\n              <div *ngIf=\"!filter; else filterOptions\"></div>\r\n            </div>\r\n            <div class=\"col-md-10\">\r\n              <canvas [hidden]=\"!filter\" id=\"appChart\">{{ chart }}</canvas>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<ng-template #noDevices>\r\n  <div class=\"alert alert-primary\" role=\"alert\">\r\n    Please select device from the list.\r\n  </div>\r\n</ng-template>\r\n\r\n<ng-template #filterOptions>\r\n  <ul class=\"list-group list-group-flush\">\r\n    <li class=\"list-group-item\">Type:<br />{{ chartType }}</li>\r\n    <li class=\"list-group-item\">From:<br />{{ filter.StartDate }}</li>\r\n    <li class=\"list-group-item\">To:<br />{{ filter.EndDate }}</li>\r\n    <li class=\"list-group-item\" *ngFor=\"let item of filter.Names\">\r\n      Device:<br />{{ item }}\r\n    </li>\r\n  </ul>\r\n  <button style=\"margin-top: 15px;\" type=\"button\" class=\"btn btn-primary btn-block\" data-dismiss=\"modal\"\r\n    data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Exports selected data to the file\"\r\n    (click)=\"onDataExport()\">Export data</button>\r\n</ng-template>\r\n"

/***/ }),

/***/ "./src/app/measurements/measurements.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/measurements/measurements.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".stretched-tabs {\n  min-width: 400px; }\n\n.large-box {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  margin: 16px;\n  padding: 16px;\n  border-radius: 8px; }\n\n.large-box {\n  height: 600px;\n  width: 1200px; }\n\n.card-padding {\n  margin: 0.5rem; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWVhc3VyZW1lbnRzL0M6XFxkZXZcXGRhZGlcXGRhZGktdWkvc3JjXFxhcHBcXG1lYXN1cmVtZW50c1xcbWVhc3VyZW1lbnRzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQix1QkFBdUI7RUFDdkIsWUFBWTtFQUNaLGFBQWE7RUFDYixrQkFBa0IsRUFBQTs7QUFHcEI7RUFDRSxhQUFhO0VBQ2IsYUFBYSxFQUFBOztBQUdmO0VBQ0UsY0FBYyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvbWVhc3VyZW1lbnRzL21lYXN1cmVtZW50cy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zdHJldGNoZWQtdGFicyB7XHJcbiAgbWluLXdpZHRoOiA0MDBweDtcclxufVxyXG5cclxuLmxhcmdlLWJveCB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIG1hcmdpbjogMTZweDtcclxuICBwYWRkaW5nOiAxNnB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDhweDtcclxufVxyXG5cclxuLmxhcmdlLWJveCB7XHJcbiAgaGVpZ2h0OiA2MDBweDtcclxuICB3aWR0aDogMTIwMHB4O1xyXG59XHJcblxyXG4uY2FyZC1wYWRkaW5nIHtcclxuICBtYXJnaW46IDAuNXJlbTtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/measurements/measurements.component.ts":
/*!********************************************************!*\
  !*** ./src/app/measurements/measurements.component.ts ***!
  \********************************************************/
/*! exports provided: MeasurementsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MeasurementsComponent", function() { return MeasurementsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _measurements_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./measurements.service */ "./src/app/measurements/measurements.service.ts");
/* harmony import */ var chart_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! chart.js */ "./node_modules/chart.js/dist/Chart.js");
/* harmony import */ var chart_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(chart_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _models_Dataset__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../models/Dataset */ "./src/app/models/Dataset.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _devices_devices_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../devices/devices.service */ "./src/app/devices/devices.service.ts");





/**
 * To type a variable or property as Moment in Typescript you can do this e.g.:
 * let myMoment: moment.Moment = moment("someDate");
 */


var MeasurementsComponent = /** @class */ (function () {
    function MeasurementsComponent(measurementsService, deviceService) {
        this.measurementsService = measurementsService;
        this.deviceService = deviceService;
        this.getAllMeasurements = false;
        this.measurementsArray = [];
        this.i = 0;
        this.type = 'line';
    }
    MeasurementsComponent.prototype.ngOnChanges = function (changes) {
        if (!changes.measurements.firstChange) {
            console.log('measurements change');
        }
        else {
            console.log('measurements change for the first time');
        }
    };
    MeasurementsComponent.prototype.ngDoCheck = function () {
        this.ctx = document.getElementById('appChart');
        if (this.measurements !== this.measurementsService.measurementList) {
            if (this.ctx) {
                this.measurements = this.measurementsService.measurementList;
                this.filter = this.measurementsService.filter;
                this.createChart(this.ctx, this.measurements);
            }
            else {
                alert('Context is null!');
            }
        }
    };
    MeasurementsComponent.prototype.ngOnDestroy = function () { };
    MeasurementsComponent.prototype.createDataset = function (measurements, type) {
        var results = [];
        var devicesList = [];
        this.measurementsArray = [];
        if (measurements.length === 0) {
            return;
        }
        for (var _i = 0, _a = measurements.Items; _i < _a.length; _i++) {
            var item = _a[_i];
            this.measurementsArray.push(item);
        }
        this.deviceService.setMeasurementsToExport(this.measurementsArray);
        var filteredMeasurements = this.measurementsArray.filter(function (item) { return item.MeasurementType.S === type; });
        var letters = '0123456789ABCDEF';
        this.filter.Names.forEach(function (name) {
            var filteredValue = filteredMeasurements.filter(function (item) { return item.DeviceName.S === name; });
            if (filteredValue.length > 0) {
                devicesList.push(filteredValue);
            }
            else {
                alert("There is no measurements for device: " + name);
            }
        });
        devicesList.forEach(function (item, index) {
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            var dataset = new _models_Dataset__WEBPACK_IMPORTED_MODULE_4__["Dataset"]();
            dataset.data = [];
            dataset.borderColor = color;
            dataset.fill = false;
            dataset.lineTension = 0;
            dataset.pointRadius = 0;
            dataset.borderWidth = 1;
            item.forEach(function (measurement) {
                if (item[index].DeviceName !== undefined ||
                    item[index].DeviceName !== null) {
                    dataset.name = measurement.DeviceName.S;
                    dataset.label = type + " for " + item[index].DeviceName.S;
                    var data = new _models_Dataset__WEBPACK_IMPORTED_MODULE_4__["Data"]();
                    data.x = moment__WEBPACK_IMPORTED_MODULE_5__(measurement.CreatedAt.S)
                        .utc()
                        .format('LLLL');
                    var value = measurement.MeasurementValue.S.replace(/,/g, '.');
                    data.y = parseFloat(value);
                    dataset.data.push(data);
                }
            });
            results.push(dataset);
        });
        return results;
    };
    MeasurementsComponent.prototype.addDataset = function () { };
    MeasurementsComponent.prototype.setTimeUnit = function () {
        var beginAt = new Date(this.filter.StartDate);
        var endAt = new Date(this.filter.EndDate);
        var minute = 60000;
        var hour = minute * 60;
        var day = hour * 24;
        var month = day * 30;
        var difference = endAt.getTime() - beginAt.getTime();
        if (difference <= minute) {
            this.unit = 'second';
        }
        else if (difference > minute && difference <= hour) {
            this.unit = 'minute';
        }
        else if (difference > hour && difference <= day) {
            this.unit = 'hour';
        }
        else if (difference > day && difference <= month * 2) {
            this.unit = 'day';
        }
        else {
            this.unit = 'month';
            // can be 'week' 'month' 'quarter' 'year'
        }
    };
    MeasurementsComponent.prototype.createChart = function (ctx, measurements) {
        var type = this.measurementsService.measurementType;
        this.chartType = type;
        this.setTimeUnit();
        if (this.chart) {
            this.chart.destroy();
        }
        this.chart = new chart_js__WEBPACK_IMPORTED_MODULE_3__["Chart"](ctx, {
            type: this.type,
            data: {
                datasets: this.createDataset(measurements, type)
            },
            options: {
                scales: {
                    xAxes: [
                        {
                            type: 'time',
                            time: {
                                unit: this.unit,
                                stepSize: 1,
                                min: new Date(Date.parse(this.filter.StartDate)).toUTCString(),
                                max: new Date(Date.parse(this.filter.EndDate)).toUTCString()
                            },
                            display: true,
                            distribution: 'linear',
                            offsetGridLines: true
                        }
                    ],
                    yAxes: [
                        {
                            display: true,
                            ticks: {
                                beginAtZero: true,
                                callback: function (value) {
                                    if (type === 'Temperature') {
                                        return value + ' ℃';
                                    }
                                    else if (type === 'Humidity') {
                                        return value + ' %';
                                    }
                                    else if (type === 'O2Level') {
                                        return value + ' %';
                                    }
                                    else if (type === 'CO2Level') {
                                        return value + ' ppm';
                                    }
                                    else if (type === 'COLevel') {
                                        return value + ' ppm';
                                    }
                                    else if (type === 'NO2Level') {
                                        return value + ' ppm';
                                    }
                                    else if (type === 'AirQuality') {
                                        return value + '';
                                    }
                                    else if (type === 'Luminance') {
                                        return value + ' Lux';
                                    }
                                    else if (type === 'VisibleLight') {
                                        return value + ' lm';
                                    }
                                    else if (type === 'InfraredLight') {
                                        return value + ' lm';
                                    }
                                }
                            }
                        }
                    ]
                }
            }
        });
    };
    /**
     * method that adds data to chart
     * @param chart chart we want to update
     * @param label new label to be displayed (e.g timespan) on X axes
     * @param data new data added to chart
     */
    MeasurementsComponent.prototype.addDataToChart = function (chart, label, data) {
        chart.data.labels.push(label);
        chart.data.datasets.forEach(function (dataset) {
            dataset.data.push(data);
        });
        chart.update();
    };
    /**
     * this method deletes the data from chart (FIFO)
     * @param chart chart from we want to delete data
     */
    MeasurementsComponent.prototype.removeDataFromChart = function (chart) {
        chart.data.labels.shift();
        chart.data.datasets.forEach(function (dataset) {
            dataset.data.shift();
        });
        chart.update();
    };
    MeasurementsComponent.prototype.onDataExport = function () {
        this.deviceService.onDataExport();
    };
    MeasurementsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-measurements',
            template: __webpack_require__(/*! ./measurements.component.html */ "./src/app/measurements/measurements.component.html"),
            styles: [__webpack_require__(/*! ./measurements.component.scss */ "./src/app/measurements/measurements.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_measurements_service__WEBPACK_IMPORTED_MODULE_2__["MeasurementsService"],
            _devices_devices_service__WEBPACK_IMPORTED_MODULE_6__["DevicesService"]])
    ], MeasurementsComponent);
    return MeasurementsComponent;
}());



/***/ }),

/***/ "./src/app/measurements/measurements.service.ts":
/*!******************************************************!*\
  !*** ./src/app/measurements/measurements.service.ts ***!
  \******************************************************/
/*! exports provided: MeasurementsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MeasurementsService", function() { return MeasurementsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _user_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../user/auth.service */ "./src/app/user/auth.service.ts");






var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    })
};
var MeasurementsService = /** @class */ (function () {
    function MeasurementsService(httpClient, authService) {
        this.httpClient = httpClient;
        this.authService = authService;
        this.env = _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"];
        this.url = "/measurements";
    }
    MeasurementsService.prototype.getMeasurementTypes = function () {
        var reqUrl = '';
        if (this.env.production) {
            reqUrl = this.url + "/types";
        }
        else {
            reqUrl = "" + this.env.protocol + this.env.host + this.url + "/types";
        }
        var token = '';
        this.authService.getAuthenticatedUser().getSession(function (err, session) {
            if (err) {
                return;
            }
            token = session.getIdToken();
        });
        return this.httpClient
            .get(reqUrl, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]().set('Authorization', token)
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (types) {
            return types;
        }));
    };
    /**
     *
     * @param filter options taken from device services
     */
    MeasurementsService.prototype.getMeasurementsByFilter = function (filter) {
        var reqUrl = '';
        var token = '';
        this.filter = filter;
        this.filter.Type = this.measurementType;
        if (this.env.production) {
            reqUrl = this.url + '/search';
        }
        else {
            reqUrl = "" + this.env.protocol + this.env.host + this.url + "/search";
        }
        this.authService.getAuthenticatedUser().getSession(function (err, session) {
            if (err) {
                return;
            }
            token = session.getIdToken();
        });
        httpOptions.headers.append('Authorization', token);
        return this.httpClient.post(reqUrl, this.filter, httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (measurements) {
            return measurements;
        }));
    };
    MeasurementsService.prototype.setMeasurements = function (measurements) {
        this.measurementList = measurements;
    };
    MeasurementsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            _user_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]])
    ], MeasurementsService);
    return MeasurementsService;
}());



/***/ }),

/***/ "./src/app/models/Dataset.ts":
/*!***********************************!*\
  !*** ./src/app/models/Dataset.ts ***!
  \***********************************/
/*! exports provided: Dataset, Data, DatasetMinimal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Dataset", function() { return Dataset; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Data", function() { return Data; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatasetMinimal", function() { return DatasetMinimal; });
var Dataset = /** @class */ (function () {
    function Dataset() {
    }
    return Dataset;
}());

var Data = /** @class */ (function () {
    function Data() {
    }
    return Data;
}());

var DatasetMinimal = /** @class */ (function () {
    function DatasetMinimal() {
    }
    return DatasetMinimal;
}());



/***/ }),

/***/ "./src/app/user/auth-guard.service.ts":
/*!********************************************!*\
  !*** ./src/app/user/auth-guard.service.ts ***!
  \********************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./auth.service */ "./src/app/user/auth.service.ts");




var AuthGuard = /** @class */ (function () {
    function AuthGuard(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        var isAuthenticated = this.authService.isAuthenticated();
        return isAuthenticated;
    };
    AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/user/auth.service.ts":
/*!**************************************!*\
  !*** ./src/app/user/auth.service.ts ***!
  \**************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! amazon-cognito-identity-js */ "./node_modules/amazon-cognito-identity-js/es/index.js");





var POOL_DATA = {
    UserPoolId: 'eu-central-1_ONEkcJG1F',
    ClientId: '7n69u7504ffg2s6hkovj1p8oqk'
};
var userPool = new amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_4__["CognitoUserPool"](POOL_DATA);
var sessionUserAttributes;
var AuthService = /** @class */ (function () {
    function AuthService(router) {
        this.router = router;
        this.authIsLoading = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](false);
        this.authDidFail = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](false);
        this.authStatusChanged = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
    }
    AuthService.prototype.signIn = function (username, password) {
        this.authIsLoading.next(true);
        this.userEmail = username;
        var authData = {
            Username: username,
            Password: password
        };
        var authDetails = new amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_4__["AuthenticationDetails"](authData);
        var userData = {
            Username: username,
            Pool: userPool
        };
        var cognitoUser = new amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_4__["CognitoUser"](userData);
        var self = this;
        cognitoUser.authenticateUser(authDetails, {
            onSuccess: function (result) {
                self.authStatusChanged.next(true);
                self.authDidFail.next(false);
                self.authIsLoading.next(false);
                // save data
                localStorage.setItem('dadi-user', userData.Username);
                self.router.navigate(['/app']);
            },
            onFailure: function (err) {
                self.authDidFail.next(true);
                self.authIsLoading.next(false);
                console.log(err.message);
            },
            newPasswordRequired: function (userAttributes, requiredAttributes) {
                delete userAttributes.email_verified;
                sessionUserAttributes = userAttributes;
                // temporary pass change prompt => to be replaced with modal
                var pass = prompt('Please insert new password!');
                // here logic to change password created by admin
                cognitoUser.completeNewPasswordChallenge(pass, requiredAttributes, {
                    onFailure: function (error) {
                        console.log(error);
                    },
                    onSuccess: function (result) {
                        self.authStatusChanged.next(true);
                        self.authDidFail.next(false);
                        self.authIsLoading.next(false);
                        self.router.navigate(['/app']);
                    }
                });
            }
        });
        this.authStatusChanged.next(true);
        return;
    };
    AuthService.prototype.getAuthenticatedUser = function () {
        return userPool.getCurrentUser();
    };
    AuthService.prototype.logout = function () {
        this.getAuthenticatedUser().signOut();
        // this.authStatusChanged.next(false);
        this.router.navigate(['/']);
    };
    AuthService.prototype.isAuthenticated = function () {
        var _this = this;
        var user = this.getAuthenticatedUser();
        var obs = rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"].create(function (observer) {
            if (!user) {
                _this.router.navigate(['/']);
            }
            else {
                user.getSession(function (err, session) {
                    if (err) {
                        _this.router.navigate(['/']);
                    }
                    else {
                        if (session.isValid) {
                            observer.next(true);
                        }
                        else {
                            _this.router.navigate(['/']);
                        }
                    }
                });
            }
            observer.complete();
        });
        return obs;
    };
    AuthService.prototype.initAuth = function () {
        var _this = this;
        this.isAuthenticated().subscribe(function (auth) { return _this.authStatusChanged.next(auth); });
    };
    AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/user/signin/signin.component.html":
/*!***************************************************!*\
  !*** ./src/app/user/signin/signin.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  <div class=\"row\" *ngIf=\"didFail\">\r\n    <div class=\"col-xs-12 cold-sm-offset-1 col-md-offset-3 text-center content-center\">\r\n      <div class=\"alert alert-danger\">Something went wrong, please try again!</div>\r\n    </div>\r\n  </div>\r\n  <div class=\"row\">\r\n    <form class=\"text-center jumbotron content-center\" (ngSubmit)=\"onSubmit()\" #usrForm=\"ngForm\">\r\n      <div class=\"form-group\">\r\n        <label for=\"username\" class=\"control-label\">Email</label>\r\n        <input type=\"email\" id=\"username\" name=\"username\" ngModel class=\"form-control text-center\" required>\r\n      </div>\r\n      <div class=\"form-group\">\r\n        <label for=\"password\" class=\"control-label\">Password</label>\r\n        <input type=\"password\" id=\"password\" name=\"password\" ngModel class=\"form-control text-center\" required>\r\n      </div>\r\n      <button class=\"btn btn-primary\" type=\"submit\" [disabled]=\"usrForm.invalid\">Submit</button>\r\n    </form>\r\n    <div class=\"loader\" *ngIf=\"isLoading\"></div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/user/signin/signin.component.scss":
/*!***************************************************!*\
  !*** ./src/app/user/signin/signin.component.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container {\n  padding-top: 5%; }\n\n.content-center {\n  width: 50%;\n  margin-left: 25%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9zaWduaW4vQzpcXGRldlxcZGFkaVxcZGFkaS11aS9zcmNcXGFwcFxcdXNlclxcc2lnbmluXFxzaWduaW4uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxlQUFlLEVBQUE7O0FBR2pCO0VBQ0UsVUFBVTtFQUNWLGdCQUFnQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvdXNlci9zaWduaW4vc2lnbmluLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lciB7XHJcbiAgcGFkZGluZy10b3A6IDUlO1xyXG59XHJcblxyXG4uY29udGVudC1jZW50ZXIge1xyXG4gIHdpZHRoOiA1MCU7XHJcbiAgbWFyZ2luLWxlZnQ6IDI1JTtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/user/signin/signin.component.ts":
/*!*************************************************!*\
  !*** ./src/app/user/signin/signin.component.ts ***!
  \*************************************************/
/*! exports provided: SigninComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SigninComponent", function() { return SigninComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../auth.service */ "./src/app/user/auth.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");




var SigninComponent = /** @class */ (function () {
    function SigninComponent(authService) {
        this.authService = authService;
        this.didFail = false;
        this.isLoading = false;
    }
    SigninComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.authIsLoading.subscribe(function (isLoading) { return (_this.isLoading = isLoading); });
        this.authService.authDidFail.subscribe(function (didFail) { return (_this.didFail = didFail); });
    };
    SigninComponent.prototype.onSubmit = function () {
        var usrName = this.form.value.username;
        var password = this.form.value.password;
        this.authService.signIn(usrName, password);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('usrForm'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgForm"])
    ], SigninComponent.prototype, "form", void 0);
    SigninComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-signin',
            template: __webpack_require__(/*! ./signin.component.html */ "./src/app/user/signin/signin.component.html"),
            styles: [__webpack_require__(/*! ./signin.component.scss */ "./src/app/user/signin/signin.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], SigninComponent);
    return SigninComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    name: 'DADI dev mode',
    protocol: 'https://',
    host: '5v5p7mxm43.execute-api.eu-central-1.amazonaws.com/dev',
    port: 8080
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_4__);





if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\dev\dadi\dadi-ui\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map