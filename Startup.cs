﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Reflection;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;

using Amazon;
using Amazon.DynamoDBv2;
using Amazon.Extensions.NETCore.Setup;

using Newtonsoft.Json;

using SensorsApi.Interfaces;
using SensorsApi.Middlewares;
using SensorsApi.Services;
using SensorsApi.Mappers;
using SensorsApi.Repositories;

using Swashbuckle.AspNetCore.Swagger;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.WebSockets.Internal;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Filters;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Cors.Internal;

namespace SensorsApi
{

    public class Startup
    {
        private readonly ILogger _logger;

        public Startup(IConfiguration configuration, IHostingEnvironment env, ILogger<Startup> logger)
        {
            Configuration = configuration;
            CurrentEnvironment = env;
            _logger = logger;
        }

        public IConfiguration Configuration { get; }

        private IHostingEnvironment CurrentEnvironment { get; set; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            _logger.LogInformation("Log test Configure Services");

            services.AddSingleton<ITimeService, TimeService>();

            services.AddSingleton<IDevicesRepository, DevicesRepository>();
            services.AddSingleton<IDeviceService, DeviceService>();

            services.AddSingleton<IMeasurementsRepository, MeasurementsRepository>();
            services.AddSingleton<IMeasurementService, MeasurementService>();

            services.AddTransient<IMapper, Mapper>();

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllOrigins",
                    builder =>
                    {
                        builder.WithOrigins("*")
                            .AllowAnyMethod()
                            .AllowAnyHeader()
                            .AllowCredentials();
                    }
                );                
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);


            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("AllowAllOrigins"));
            });

            #region AWS Options
            
            services.AddAWSService<IAmazonDynamoDB>(); // Added AWS service for Dependency Injection
            services.AddSingleton<IDevicesRepository, DevicesRepository>();
            services.AddDefaultAWSOptions(
                new AWSOptions
                {
                    Region = RegionEndpoint.GetBySystemName(Configuration["AWSSettings:AWSRegion"]) // Set region where are all aws services are hosted
                });

            #endregion

            services.AddSwaggerGen(swagger =>
            {
                swagger.SwaggerDoc("v1", new Info
                {
                    Title = "DaDi API",
                    Version = "v1",
                    Description = "API server for DaDi UI and devices"
                });
                swagger.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "Standard Authorization header using the Bearer scheme. Example: \"bearer {token}\"",
                    In = "header",
                    Name = "Authorize",
                    Type = "apiKey"
                });

                swagger.OperationFilter<Middlewares.SecurityRequirementsOperationFilter>();
                //swagger.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> { { "Bearer", Enumerable.Empty<string>() } });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                swagger.IncludeXmlComments(xmlPath);
                swagger.CustomSchemaIds(x => x.FullName);
            });
           
            //Device Authorization
            services.AddAuthorization(options =>
            {
                options.AddPolicy("HasApiKey", policy => policy.Requirements.Add(new HasKeyRequirement()));
            });
            
            //UI Authorization
            services.AddAuthentication("Bearer")
                .AddJwtBearer(options =>
            {
                options.Audience = Configuration["AWSSettings:Cognito:Audience"];
                options.Authority = Configuration["AWSSettings:Cognito:Authority"];
            });


            services.AddSingleton<IAuthorizationHandler, HasKeyHandler>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            /*
             Exception/error handling
             HTTP Strict Transport Security Protocol
             HTTPS redirection
             Static file server
             Cookie policy enforcement
             Authentication
             Session
             MVC
             */

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            //app.UseHttpsRedirection();
            app.LogRequestHeaders(_logger);
            app.UseOptions();
            app.UseDefaultFiles();
            app.UseStaticFiles();

            // configure the app for usage as api
            // app.UseMvcWithDefaultRoute();

            app.UseSwagger();
            app.UseSwaggerUI(swagger =>
            {
                swagger.SwaggerEndpoint("/swagger/v1/swagger.json", "DaDi API v1");
                swagger.RoutePrefix = "swagger/ui";
            });


            app.UseAuthentication();
            app.UseMvc();
            app.UseCors(builder => builder
                .AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowCredentials());
        }
    }
}