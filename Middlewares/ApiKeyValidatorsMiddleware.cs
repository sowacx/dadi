﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using SensorsApi.Interfaces;
using SensorsApi.Models;
using SensorsApi.Services;

namespace SensorsApi.Middlewares
{

    public static class SchemesNamesConst
    {
        public const string SchemesNamesConstant = "TokenAuthenticationScheme";
    }

    public class HasKeyRequirement : IAuthorizationRequirement
    {        
        //public IDeviceService DeviceService;

        public HasKeyRequirement()
        {
            
        }


    }


    public class HasKeyHandler : AuthorizationHandler<HasKeyRequirement>
    {
        IDeviceService DeviceService;

        public HasKeyHandler(IDeviceService deviceService)
        {
            DeviceService = deviceService;
        }

        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, HasKeyRequirement requirement)
        {
            //IDeviceService deviceService = new DeviceService();
            var devices = await DeviceService.GetDevices();

            var httpContext = ((AuthorizationFilterContext)context.Resource).HttpContext;

            foreach (var item in httpContext.Request.Headers)
            {
                Console.WriteLine($"Header: {item.Key} Value: {item.Value}");
            }


            if (httpContext.Request.Headers.ContainsKey("ApiKey") &&
                httpContext.Request.Headers.ContainsKey("DeviceName"))                
            {
                string apiKey = httpContext.Request.Headers["ApiKey"].ToString();
                string deviceName= httpContext.Request.Headers["DeviceName"].ToString();

                if (devices.Exists(d => d.DeviceName.Equals(deviceName, StringComparison.Ordinal)
                 && d.Token.Equals(apiKey, StringComparison.Ordinal)))
                {
                    context.Succeed(requirement);
                }
            }

            if(!context.HasSucceeded) context.Fail();                        
        }
    }
}
