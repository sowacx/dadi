﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Builder;

namespace SensorsApi.Middlewares
{
    public class BasicAuthMiddleware
    {
        private readonly RequestDelegate _next;

        public BasicAuthMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            if (!context.User.Identity.IsAuthenticated)
            {
                var basicAuthenticationHeader = GetBasicAuthencationHeaderValue(context);
                if (basicAuthenticationHeader.IsValidBasicAuthHeaderValue)
                {
                    var authenticationManager = new BasicAuthSignInManager(context, basicAuthenticationHeader);
                    await authenticationManager.TrySignInUser();
                }
            }
            await _next.Invoke(context);
        }

        private BasicAuthHeaderValue GetBasicAuthencationHeaderValue(HttpContext context)
        {
            var basicAuthenticationHeader = context.Request.Headers["Authorization"].FirstOrDefault(header => header.StartsWith("Basic", StringComparison.OrdinalIgnoreCase));
            return new BasicAuthHeaderValue(basicAuthenticationHeader);
        }
    }

    public static class BasicAuthExtension
    {
        public static IApplicationBuilder ApplyBasicAuth(this IApplicationBuilder app)
        {
            app.UseMiddleware<BasicAuthMiddleware>();
            return app;
        }
    }
}
