﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace SensorsApi.Middlewares
{
    public class BasicAuthSignInManager
    {
        private readonly HttpContext _context;
        private readonly BasicAuthHeaderValue _authHeadreValue;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;

        private IdentityUser _user;

        public BasicAuthSignInManager(
            HttpContext context, 
            BasicAuthHeaderValue authHeaderValue)
        {
            _context = context;
            _authHeadreValue = authHeaderValue;
            //_userManager = userManager;
            //_signInManager = signInManager;
            _userManager = _context.RequestServices.GetRequiredService<UserManager<IdentityUser>>();
            _signInManager = _context.RequestServices.GetRequiredService<SignInManager<IdentityUser>>();
        }

        public async Task TrySignInUser()
        {
            if (_authHeadreValue.IsValidBasicAuthHeaderValue)
            {
                await GetUserByUsernameOrEmail();
                if (_user != null)
                {
                    await SignInUserIfPasswordIsCorrect();
                }
            }
        }

        private async Task GetUserByUsernameOrEmail()
        {
            _user = await _userManager.FindByEmailAsync(_authHeadreValue.UserIdentifier) ?? await _userManager.FindByNameAsync(_authHeadreValue.UserIdentifier);
        }

        private async Task SignInUserIfPasswordIsCorrect()
        {
            if (await _userManager.CheckPasswordAsync(_user, _authHeadreValue.UserPassword))
            {
                _context.User = await _signInManager.CreateUserPrincipalAsync(_user);
            }
        }
    }
}
