﻿
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Controllers;
using System.Linq;
using Microsoft.AspNetCore.JsonPatch.Operations;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.AspNetCore.Authorization;
using Swashbuckle.AspNetCore.Swagger;
//using Microsoft.OpenApi.Models;

namespace SensorsApi.Middlewares
{
    public class SecurityRequirementsOperationFilter : IOperationFilter
    {
        public void Apply(Swashbuckle.AspNetCore.Swagger.Operation operation, OperationFilterContext context)
        {
            if (!context.MethodInfo.GetCustomAttributes(true).Any(_ => _ is AllowAnonymousAttribute))
            {
                operation.Security = new List<IDictionary<string, IEnumerable<string>>>
                {
                    new Dictionary<string, IEnumerable<string>>
                    {
                        {"Bearer", Array.Empty<string>()}
                    }
                };
            }
        }
    }
}
