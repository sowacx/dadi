﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorsApi.Middlewares
{
    public static class IApplicationBuilderExtensions
    {
        /// <summary>
        /// Logs all request headers
        /// </summary>
        /// <param name="app"><see cref="IApplicationBuilder"/></param>
        /// <param name="loggerFactory"<see cref="ILoggerFactory"/></param>
        public static void LogRequestHeaders(this IApplicationBuilder app, ILogger logger)
        {            
            app.Use(async (context, next) =>
            {
                var builder = new StringBuilder(Environment.NewLine);
                foreach (var header in context.Request.Headers)
                {
                    builder.AppendLine($"{header.Key}:{header.Value}");
                }
                logger.LogTrace(builder.ToString());
                await next.Invoke();
            });
        }
    }
}
