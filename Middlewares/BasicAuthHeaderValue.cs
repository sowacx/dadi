﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SensorsApi.Middlewares
{
    public class BasicAuthHeaderValue
    {
        private readonly string _authHeaderValue;
        private string[] _splitDecodedCredentials;

        public bool IsValidBasicAuthHeaderValue { get; private set; }
        public string UserIdentifier { get; private set; }
        public string UserPassword { get; private set; }

        public BasicAuthHeaderValue(string authHeaderValue)
        {
            
            if (!string.IsNullOrWhiteSpace(authHeaderValue))
            {
                _authHeaderValue = authHeaderValue;
                if (TryDecodeValue())
                {
                    ReadAuthnHeaderValue();
                }
            }
        }

        private bool TryDecodeValue()
        {
            const int headerSchemeLength = 6; // "Basic " => and the encoded value
            if (_authHeaderValue.Length <= headerSchemeLength) return false;

            var encodedCredentials = _authHeaderValue.Substring(headerSchemeLength);

            try
            {
                var decodedCredentials = Convert.FromBase64String(encodedCredentials);
                _splitDecodedCredentials = System.Text.Encoding.ASCII.GetString(decodedCredentials).Split(':');
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private void ReadAuthnHeaderValue()
        {
            IsValidBasicAuthHeaderValue = _splitDecodedCredentials.Length == 2 && 
                                          !string.IsNullOrWhiteSpace(_splitDecodedCredentials[0]) && 
                                          !string.IsNullOrWhiteSpace(_splitDecodedCredentials[1]);

            if (IsValidBasicAuthHeaderValue)
            {
                UserIdentifier = _splitDecodedCredentials[0];
                UserPassword = _splitDecodedCredentials[1];
            }
        }
    }
}
