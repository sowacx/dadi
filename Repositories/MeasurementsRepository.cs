﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;

using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using Microsoft.Extensions.Configuration;
using SensorsApi.Interfaces;
using SensorsApi.Models;

namespace SensorsApi.Repositories
{
    public class MeasurementsRepository : IMeasurementsRepository
    {
        private const string TableName = "Dadi";
        private readonly IAmazonDynamoDB _dynamoDbClient;
        private readonly IConfiguration _config;

        public MeasurementsRepository(IAmazonDynamoDB dynamoDbClient, IConfiguration config)
        {
            try
            {
                _dynamoDbClient = dynamoDbClient;
                _config = config;
            }
            catch (Exception ex)
            {
                Console.WriteLine("     FAILED to create a DynamoDB client; " + ex.Message);
            }
        }

        public async Task<IEnumerable<Dictionary<string, AttributeValue>>> GetMeasurements(string device, string type, string startDate, string endDate)
        {
            var request = new ScanRequest
            {
                TableName = TableName,
                ExpressionAttributeValues = new Dictionary<string, AttributeValue>
                {
                    { ":v_name", new AttributeValue { S = device } },
                    { ":v_start", new AttributeValue { S = startDate } },
                    { ":v_end", new AttributeValue { S = endDate } },
                    { ":v_type", new AttributeValue { S = type } }
                },
                FilterExpression = "DeviceName = :v_name and CreatedAt between :v_start and :v_end and MeasurementType = :v_type"
            };

            var response = await _dynamoDbClient.ScanAsync(request);
            return response.Items;
        }

        public async Task<IEnumerable<Dictionary<string, AttributeValue>>> GetTypes()
        {
            var request = new ScanRequest
            {
                TableName = TableName,
                ProjectionExpression = "MeasurementType"
            };

            var response = await _dynamoDbClient.ScanAsync(request);
            return response.Items;
        }

        public async Task<PutItemResponse> CreateMeasurement(MeasurementAWS measurementAWS)
        {
            //TimeToLive for single document at DynamoDB 
            int ttl = _config.GetValue<int>("AWSSettings:DurationDaysTTL");
            long ttlEpoc = DateTimeOffset.Now.AddDays(ttl).ToUnixTimeSeconds();
            var request = new PutItemRequest
            {
                TableName = TableName,
                Item = new Dictionary<string, AttributeValue>()
                {
                    { "DeviceName", new AttributeValue { S = measurementAWS.DeviceName } },
                    { "CreatedAt", new AttributeValue { S = measurementAWS.CreatedAt } },
                    { "MeasurementType", new AttributeValue { S = measurementAWS.MeasurementType } },
                    { "MeasurementValue", new AttributeValue { S = measurementAWS.MeasurementValue } },
                    { "DeviceNameMeasurementType", new AttributeValue { S = measurementAWS.DeviceName + measurementAWS.MeasurementType } },
                    { "TTL", new AttributeValue { S = ttlEpoc.ToString() } }
                }
            };

            return await _dynamoDbClient.PutItemAsync(request);
        }
    }
}
