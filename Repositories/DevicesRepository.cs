﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using SensorsApi.Interfaces;
using SensorsApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SensorsApi.Repositories
{
    public class DevicesRepository : IDevicesRepository
    {
        private const string TABLE_NAME = "Dadi_Devices_Tokens";
        private readonly IAmazonDynamoDB _dynamoDbClient;
        private DynamoDBContext context;

       // private List<DeviceAWS> devicesList = 

        private List<DeviceAWS> DevicesList = new List<DeviceAWS>();
       

        public DevicesRepository(IAmazonDynamoDB dynamoDbClient)
        {
            try
            {
                _dynamoDbClient = dynamoDbClient;
                context = new DynamoDBContext(_dynamoDbClient);
            }
            catch (Exception ex)
            {
                Console.WriteLine("     FAILED to create a DynamoDB client; " + ex.Message);
            }
        }       

        public Task<List<DeviceAWS>> GetDevices()
        {
            if (DevicesList.Count > 0) return Task<List<DeviceAWS>>.Factory.StartNew(() => { return this.DevicesList; });

            var conditions = new List<ScanCondition>();            
            var search = context.FromScanAsync<DeviceAWS>(new Amazon.DynamoDBv2.DocumentModel.ScanOperationConfig()
            {
                ConsistentRead = true,
            }, new DynamoDBOperationConfig()
            {
                OverrideTableName = TABLE_NAME
            }); ;

            var searchResponse = search.GetRemainingAsync();
            searchResponse.ContinueWith(cw => this.DevicesList = cw.Result);
            //searchResponse.Wait();

            return searchResponse;
        }

        public Task InsertDevice(DeviceAWS device)
        {            
            return context.SaveAsync<DeviceAWS>(device, new DynamoDBOperationConfig()
            {
                OverrideTableName = TABLE_NAME
            });
        }

        public Task DeleteDevice(DeviceAWS device)
        {                        
            return context.DeleteAsync<DeviceAWS>(device, new DynamoDBOperationConfig()
            {
                OverrideTableName = TABLE_NAME
            });
        }

        public void Refresh()
        {
            DevicesList = new List<DeviceAWS>();
        }
    }
}
